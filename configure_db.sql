CREATE EXTENSION dblink;

DO
$do$
BEGIN
	--CREATE EXTENSION IF NOT EXISTS dblink;

	IF NOT EXISTS (SELECT 1 FROM pg_database WHERE datname = 'cashflow') THEN
	   PERFORM dblink_exec('dbname=' || current_database(), 'CREATE DATABASE cashflow');
	END IF;

	IF NOT EXISTS (SELECT * FROM   pg_catalog.pg_user WHERE  usename = 'cashflow') THEN
      CREATE ROLE cashflow LOGIN PASSWORD 'cashflow';
      GRANT ALL PRIVILEGES ON DATABASE "cashflow" to cashflow;
      ALTER USER cashflow CREATEDB;
    END IF;

END
$do$ LANGUAGE plpgsql;
