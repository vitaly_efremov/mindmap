from django.views.generic.base import TemplateView


class LoginMockView(TemplateView):
    template_name = "mock/login.html"


class SignUpMockView(TemplateView):
    template_name = "mock/signup.html"
