from django.conf.urls import url

from mindmap.views.app.dashboard import IndexView
from mindmap.views.app_mocks import LoginMockView, SignUpMockView

urlpatterns = [
    url(r'^$', IndexView.as_view()),
]

# Mocks are here
urlpatterns += [
    url(r'^mock/login/$', LoginMockView.as_view()),
    url(r'^mock/signup/$', SignUpMockView.as_view()),
]
